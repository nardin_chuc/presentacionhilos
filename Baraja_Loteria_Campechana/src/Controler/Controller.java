package Controler;

import Vista.MenuBaraja;
import java.applet.AudioClip;
import java.awt.Color;
import static java.awt.Color.BLACK;
import static java.awt.Color.BLUE;
import static java.awt.Color.RED;
import static java.awt.Color.WHITE;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Array;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Controller implements Runnable, ActionListener, MouseListener {

    Thread hiloImagen, hiloNumero, hiloMusica, hiloAntImagen,hiloMusicFondo, HiloFinal;
    JTextField[] numRecibidos = new JTextField[90];
    int[] numeros = new int[90];
    int NumeroEncontrado = 0, NumeroAnterior = 0;
    int aux, contador = 0;

    private MenuBaraja mb;

    public Controller(MenuBaraja mb) {
        this.mb = mb;
        mb.jButton_Iniciar.addActionListener(this);
        mb.jButton_Reiniciar.addActionListener(this);
        mb.jButton_Loteria.addActionListener(this);
        mb.jButton_Barajear.addActionListener(this);
        mb.jButton_Barajear.addMouseListener(this);
        mb.jButton_Iniciar.addMouseListener(this);
        mb.jButton_Reiniciar.addMouseListener(this);
        mb.jButton_Loteria.addMouseListener(this);
        verPaneles(true);
        inicio(false,false,true);
        mb.setLocationRelativeTo(null);
    }

    @Override
    public void run() {
        Thread ct = Thread.currentThread();
        if (ct == hiloImagen) {
            try {
                
                cambiarImagen(NumeroEncontrado);
                mb.jButton_Iniciar.setEnabled(false);
                Thread.sleep(4000);
                mb.jButton_Iniciar.setEnabled(true);
                Thread.sleep(1000);
            } catch (Throwable e) {
                JOptionPane.showMessageDialog(null, "Error " + e.getMessage());
            }

        } else if (ct == hiloMusica) {
            try {
                Thread.sleep(800);
                cambiarMusica(NumeroEncontrado, hiloMusica,4000);
            } catch (Throwable e) {
                JOptionPane.showMessageDialog(null, "Error " + e.getMessage());
            }
        } else if (ct == hiloNumero) {
            try {
                imprimirLabelNumero(NumeroEncontrado);
                Thread.sleep(1000);
            } catch (Throwable e) {
               // JOptionPane.showMessageDialog(null, "Error " + e.getMessage());
            }
        } else if (ct == hiloAntImagen) {
            try {
                imagenAnterior(NumeroAnterior);
                Thread.sleep(1000);
            } catch (Throwable e) {
                JOptionPane.showMessageDialog(null, "Error " + e.getMessage());
            }
        }else if (ct == hiloMusicFondo) {
            try {
                cambiarMusica(0,hiloMusicFondo,500);
                Thread.sleep(1000);
            } catch (Throwable e) {
               // JOptionPane.showMessageDialog(null, "Error 1" + e.getMessage());
            }
        }
        else if (ct == HiloFinal) {
            try {
                cambiarMusica(91,HiloFinal,2500);
                Thread.sleep(1000);
            } catch (Throwable e) {
               // JOptionPane.showMessageDialog(null, "Error 1" + e.getMessage());
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == mb.jButton_Iniciar) {
            inicio(true, true,false);
            hiloImagen = new Thread(this);
            hiloNumero = new Thread(this);
            hiloMusica = new Thread(this);
            hiloAntImagen = new Thread(this);
            hiloMusicFondo = new Thread(this);
            
                if (contador == 89) {
                    mb.jButton_Iniciar.setEnabled(false);
                } else {
                    verPaneles(true);
                    meterNumero(contador);
                try {
                    hiloImagen.start();
                    hiloNumero.start();
                    hiloAntImagen.start();
                    hiloMusicFondo.start();
                    hiloMusica.start();
                    Thread.sleep(100);
                    hiloAntImagen.suspend();
                    hiloNumero.suspend();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
                    
                    
                }
                contador++;
            
        } else if (e.getSource() == mb.jButton_Reiniciar) {
            inicio(false,false,true);
            cambiarMusica(0,hiloMusica,3000);
            limpiar("Reiniciando juego");
        }else if (e.getSource() == mb.jButton_Loteria) {
            HiloFinal = new Thread(this);
            HiloFinal.start();
            inicio(false,false,true);
            limpiar("¡¡¡¡¡¡LOTERIAAAA!!!!!");
        } else if (e.getSource() == mb.jButton_Barajear) {
            cambiarMusica(0,hiloMusica,3000);
            inicio(true, false,false);
        }           

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == mb.jButton_Iniciar) {
            mb.jLabel_AvisoIniciar.setVisible(true);
            mb.jLabel_AvisoIniciar.setText("Siguiente Carta");
            mb.jButton_Iniciar.setForeground(WHITE);
            mb.jButton_Iniciar.setBackground(BLUE);
        } else if (e.getSource() == mb.jButton_Loteria) {
            mb.jLabel_AvisoLoteria.setVisible(true);
            mb.jButton_Loteria.setForeground(WHITE);
            mb.jButton_Loteria.setBackground(BLUE);
            mb.jLabel_AvisoLoteria.setText("Click para el Ganador");
        } else if (e.getSource() == mb.jButton_Reiniciar) {
            mb.jButton_Reiniciar.setForeground(WHITE);
            mb.jLabel_AvisoReiniciar.setVisible(true);
            mb.jButton_Reiniciar.setBackground(BLUE);
            mb.jLabel_AvisoReiniciar.setText("Reinicia el juego");
        }else if (e.getSource() == mb.jButton_Barajear ) {
            mb.jButton_Barajear.setForeground(WHITE);
            mb.jLabel_barajear.setVisible(true);
            mb.jButton_Barajear.setBackground(BLUE);
            mb.jLabel_barajear.setText("Inicia el juego");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == mb.jButton_Iniciar) {
            mb.jLabel_AvisoIniciar.setVisible(false);
            mb.jButton_Iniciar.setBackground(Color.LIGHT_GRAY);
            mb.jButton_Iniciar.setForeground(BLACK);
        } else if (e.getSource() == mb.jButton_Loteria) {
            mb.jLabel_AvisoLoteria.setVisible(false);
            mb.jButton_Loteria.setBackground(Color.LIGHT_GRAY);
            mb.jButton_Loteria.setForeground(BLACK);
        } else if (e.getSource() == mb.jButton_Reiniciar) {
            mb.jLabel_AvisoReiniciar.setVisible(false);
            mb.jButton_Reiniciar.setBackground(Color.LIGHT_GRAY);
            mb.jButton_Reiniciar.setForeground(BLACK);
        }else if (e.getSource() == mb.jButton_Barajear ) {
            mb.jButton_Barajear.setForeground(BLACK);
            mb.jLabel_barajear.setVisible(false);
            mb.jButton_Barajear.setBackground(Color.LIGHT_GRAY);
        }
    }

    public void buscarNumero(int numero) {
        for (int i = 0; i < contador; i++) {
            if (numeros[i] == numero || numero > 91) {
                meterNumero(contador);
            } else {
            }
        }
    }

    public void meterNumero(int contador) {
        aux = (int) Math.floor(Math.random() * 90);
        if (aux == 0) {
            aux = aux + 1;
        } 
         for (int i = contador; i < contador + 1; i++) {
                numeros[i] = aux;
                buscarNumero(aux);
                NumeroEncontrado = numeros[contador];
                imprimirNumeros(numeros, contador);
                if(contador > 0){
                    NumeroAnterior = numeros[contador-1];
                }
            }
    }

    private void cambiarImagen(int NUMERO) {
        try {
            ImageIcon wallpaper = new ImageIcon("src/Imag/" + NumeroEncontrado + ".png");
            Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(mb.jLabel_MainImagen.getWidth(), mb.jLabel_MainImagen.getHeight(), Image.SCALE_DEFAULT));
            mb.jLabel_MainImagen.setIcon(icono);
            
        } catch (Throwable e) {
            // JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }

    }

    private void cambiarMusica(int Numero, Thread hilo,int tiempo) {
        try {
            AudioClip Sound;
            Sound = java.applet.Applet.newAudioClip(getClass().getResource("/Musica/" + Numero + ".wav"));
            Sound.play();
            Thread.sleep(tiempo);
            Sound.stop();
            hilo.suspend();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null, "Error en: "+e.getMessage());
        }

    }

    private void imprimirNumeros(int numeros[], int contador) {
        if (contador <= 19) {
            mb.jPanel_num_Aparecidos.removeAll();
            for (int i = 0; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                mb.jPanel_num_Aparecidos.add(numRecibidos[i]);
                mb.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    mb.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    mb.pack();
                }
            }
        } else if (contador >= 20 && contador <= 39) {
            mb.jPanel_num_Aparecidos1.removeAll();
            for (int i = 20; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                mb.jPanel_num_Aparecidos1.add(numRecibidos[i]);
                mb.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(RED);
                    numRecibidos[i].setForeground(WHITE);
                    mb.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    mb.pack();
                }
            }
        } else if (contador >= 40 && contador <= 59) {
            mb.jPanel_num_Aparecidos2.removeAll();
            for (int i = 40; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                mb.jPanel_num_Aparecidos2.add(numRecibidos[i]);
                mb.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    mb.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    mb.pack();
                }
            }
        } else if (contador >= 60 && contador <= 79) {
            mb.jPanel_num_Aparecidos3.removeAll();
            for (int i = 60; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                mb.jPanel_num_Aparecidos3.add(numRecibidos[i]);
                mb.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(RED);
                    numRecibidos[i].setForeground(WHITE);
                    mb.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    mb.pack();
                }
            }
        } else if (contador >= 80 && contador <= 89) {
            mb.jPanel_num_Aparecidos4.removeAll();
            for (int i = 80; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                mb.jPanel_num_Aparecidos4.add(numRecibidos[i]);
                mb.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    mb.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    mb.pack();
                }
            }
        }

    }

    private void imprimirLabelNumero(int NumeroEncontrado) {
        if(NumeroEncontrado > 0){
            mb.jLabel_numero.setText("" + NumeroEncontrado);
        }else{
            mb.jLabel_numero.setText("");
        }
    }

    private void imagenAnterior(int NumeroAnterior) {
       try {
            ImageIcon wallpaper = new ImageIcon("src/Imag/" + NumeroAnterior + ".png");
            Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(mb.jLabel_MainImagen.getWidth(), mb.jLabel_MainImagen.getHeight(), Image.SCALE_DEFAULT));
            mb.jLabel_CuadroImagAnterior.setIcon(icono);

        } catch (Throwable e) {
            // JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
    }

    public void limpiarPaneles(){
        mb.jPanel_num_Aparecidos.removeAll();
        mb.jPanel_num_Aparecidos1.removeAll();
        mb.jPanel_num_Aparecidos2.removeAll();
        mb.jPanel_num_Aparecidos3.removeAll();
        mb.jPanel_num_Aparecidos4.removeAll();
    }
    public void verPaneles(boolean ver){
        mb.jPanel_num_Aparecidos.setVisible(ver);
        mb.jPanel_num_Aparecidos1.setVisible(ver);
        mb.jPanel_num_Aparecidos2.setVisible(ver);
        mb.jPanel_num_Aparecidos3.setVisible(ver);
        mb.jPanel_num_Aparecidos4.setVisible(ver);
    }
    
    public void LimpiarArray(int numero[]){
        for(int i = 0; i < numero.length;i++){
            numero[i] = 0;
        }
    }

    private void limpiar(String texto) {
            hiloImagen = new Thread(this);
            hiloNumero = new Thread(this);
            hiloAntImagen = new Thread(this);
            JOptionPane.showMessageDialog(null,texto);
            verPaneles(false);
            limpiarPaneles();
            LimpiarArray(numeros);
            NumeroEncontrado = 0;
            NumeroAnterior = 0;
            hiloImagen.start();
            hiloAntImagen.start();
            hiloNumero.start();
            cambiarImagen(NumeroEncontrado);
            imprimirLabelNumero(NumeroEncontrado);
            imagenAnterior(NumeroAnterior);
            hiloNumero.suspend();
            hiloImagen.suspend();
            hiloMusica.stop();
            hiloAntImagen.suspend();
            contador = 0;
    }
    
    public void inicio(boolean ver, boolean reinicio, boolean baraja){
        mb.jButton_Reiniciar.setVisible(reinicio);
        mb.jButton_Loteria.setVisible(reinicio);
        mb.jButton_Iniciar.setVisible(ver);
        mb.jButton_Barajear.setVisible(baraja);
    }
    
}
