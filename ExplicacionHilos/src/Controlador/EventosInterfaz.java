package Controlador;

import Vistas.InterfazHilos;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author CCNAR
 */
public class EventosInterfaz implements MouseListener {

    private InterfazHilos interHilos;

    public EventosInterfaz(InterfazHilos interHilos) {
        this.interHilos = interHilos;
        this.interHilos.jButtonPower.addMouseListener(this);
        this.interHilos.jButton_Cronometro.addMouseListener(this);
        this.interHilos.jButton_Word.addMouseListener(this);
        this.interHilos.jButton_Excel.addMouseListener(this);
        this.interHilos.jButton_Txt1.addMouseListener(this);
        this.interHilos.jButton_Cronometro.addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == interHilos.jButtonPower) {
            interHilos.JP_Power.setBackground(new Color(255, 0, 0));
        } else if (e.getSource() == interHilos.jButton_Cronometro) {
            interHilos.JP_Crono.setBackground(new Color(255, 0, 0));
        } else if (e.getSource() == interHilos.jButton_Word) {
            interHilos.JP_Word.setBackground(new Color(255, 0, 0));
        } else if (e.getSource() == interHilos.jButton_Txt1) {
            interHilos.JPanel_Txt1.setBackground(new Color(255, 0, 0));
        } else if (e.getSource() == interHilos.jButton_Excel) {
            interHilos.JP_Excel.setBackground(new Color(255, 0, 0));
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == interHilos.jButtonPower) {
            interHilos.JP_Power.setBackground(new Color(255, 255, 255));
        } else if (e.getSource() == interHilos.jButton_Cronometro) {
            interHilos.JP_Crono.setBackground(new Color(255, 255, 255));
        } else if (e.getSource() == interHilos.jButton_Word) {
            interHilos.JP_Word.setBackground(new Color(255, 255, 255));
        } else if (e.getSource() == interHilos.jButton_Txt1) {
            interHilos.JPanel_Txt1.setBackground(new Color(255, 255, 255));
        } else if (e.getSource() == interHilos.jButton_Excel) {
            interHilos.JP_Excel.setBackground(new Color(255, 255, 255));
        }
    }

}
