
package Controlador;

import Hilos.HiloAbrirSoftware;
import Hilos.HiloHora;
import Vistas.Cronometro;
import Vistas.InterfazHilos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CCNAR
 */
public class ControladorInterfaz implements ActionListener {
    
    String DireccionWord = "C:\\Program Files\\Microsoft Office\\root\\Office16\\WINWORD.exe";
    String DireccionPower = "C:\\Program Files\\Microsoft Office\\root\\Office16\\POWERPNT.exe";
    String DireccionExcel = "C:\\Program Files\\Microsoft Office\\root\\Office16\\EXCEL.exe";
    String DireccionTxt = "C:\\Users\\CCNAR\\AppData\\Local\\Programs\\Opera\\launcher.exe";
    
    private InterfazHilos interHilos;
    HiloAbrirSoftware HiloSoft = new HiloAbrirSoftware();
    Thread Hilo;
    HiloHora HiloHora;
    
    public ControladorInterfaz(InterfazHilos interHilos) {
        this.interHilos = interHilos;
        this.interHilos.jButtonPower.addActionListener(this);
        this.interHilos.jButton_Cronometro.addActionListener(this);
        this.interHilos.jButton_Word.addActionListener(this);
        this.interHilos.jButton_Excel.addActionListener(this);
        this.interHilos.jButton_Txt1.addActionListener(this);
        HiloHora = new HiloHora(interHilos.Lb_Reloj);
        HiloHora.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == interHilos.jButtonPower) {
            try {
                HiloSoft.setPrograma(DireccionPower);
                Hilo = new Thread(HiloSoft);
                Hilo.start();
                Thread.sleep(1000);
                Hilo.suspend();
            } catch (InterruptedException ex) {
                Logger.getLogger(ControladorInterfaz.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource() == interHilos.jButton_Cronometro) {
           Cronometro cr =  new Cronometro();
           cr.setVisible(true);
            
        } else if (e.getSource() == interHilos.jButton_Word) {
            try {
                HiloSoft.setPrograma(DireccionWord);
                Hilo = new Thread(HiloSoft);
                Hilo.start();
                Thread.sleep(1000);
                Hilo.suspend();
            } catch (InterruptedException ex) {
                Logger.getLogger(ControladorInterfaz.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource() == interHilos.jButton_Txt1) {
            try {
                HiloSoft.setPrograma(DireccionTxt);
                Hilo = new Thread(HiloSoft);
                Hilo.start();
                Thread.sleep(1000);
                Hilo.suspend();
            } catch (InterruptedException ex) {
                Logger.getLogger(ControladorInterfaz.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        } else if (e.getSource() == interHilos.jButton_Excel) {
            HiloSoft.setPrograma(DireccionExcel);
            Hilo = new Thread(HiloSoft);
            Hilo.start();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ControladorInterfaz.class.getName()).log(Level.SEVERE, null, ex);
            }
                Hilo.suspend();
            
        }
    }
    
}
