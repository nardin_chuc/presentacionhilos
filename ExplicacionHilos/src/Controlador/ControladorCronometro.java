/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Hilos.HiloCronometro;
import Vistas.Cronometro;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author CCNAR
 */
public class ControladorCronometro implements ActionListener {

    private Cronometro cr;
    HiloCronometro HiloCrono;

    public ControladorCronometro(Cronometro cr) {
        this.cr = cr;
        this.cr.jButton_Iniciar.addActionListener(this);
        this.cr.jButton_Pausa.addActionListener(this);
        this.cr.jButton_Seguir.addActionListener(this);
        this.cr.jButton_Detener.addActionListener(this);
        Botones(true, false, false, false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cr.jButton_Iniciar) {
            Botones(false, true, false, true);
            HiloCrono = new HiloCronometro();
            HiloCrono.Jlabel(cr.jLabel_Etiqueta);
            HiloCrono.start();
        } else if (e.getSource() == cr.jButton_Pausa) {
            try {
                Botones(false, false, true, true);
                HiloCrono.suspend();
            } catch (Throwable Th) {
                System.err.print("Error: " + Th);
            }

        } else if (e.getSource() == cr.jButton_Seguir) {
            try {
                Botones(false, true, false, true);
                HiloCrono.resume();
            } catch (Throwable Th) {
                System.err.print("Error: " + Th);
            }

        } else if (e.getSource() == cr.jButton_Detener) {
            try {
                HiloCrono.LimpiarLabel();
                Botones(true, false, false, false);
                HiloCrono.stop();                                   //Mata al hilo
            } catch (Throwable Th) {
                System.err.print("Error: " + Th);
            }

        }
    }

    public void Botones(boolean ini, boolean pau, boolean seg, boolean dete) {
        cr.jButton_Iniciar.setVisible(ini);
        cr.jButton_Pausa.setVisible(pau);
        cr.jButton_Seguir.setVisible(seg);
        cr.jButton_Detener.setVisible(dete);

    }

}
