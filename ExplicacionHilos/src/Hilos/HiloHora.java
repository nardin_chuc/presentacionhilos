
package Hilos;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author CCNAR
 */
public class HiloHora extends Thread {
    
    String hora, minutos, segundos, ampm;
    Calendar calendario;
    JLabel Etiqueta;
    
    public HiloHora(JLabel EtiquetaHora){
        this.Etiqueta = EtiquetaHora;
    }
    
    @Override
    public void run(){
        try {
            while(true){
                Hora();
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(HiloHora.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void Hora(){
        Calendar calendario = new GregorianCalendar();
        Date fechaHoraActual = new Date();
        calendario.setTime(fechaHoraActual);
        ampm = calendario.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";

        if (ampm.equals("PM")) {
            int h = calendario.get(Calendar.HOUR_OF_DAY);
            hora = "" + (h-12);
        } else {
            hora = calendario.get(Calendar.HOUR_OF_DAY) > 9 ? "" + calendario.get(Calendar.HOUR_OF_DAY) : "0" + calendario.get(Calendar.HOUR_OF_DAY);
        }
        minutos = calendario.get(Calendar.MINUTE) > 9 ? "" + calendario.get(Calendar.MINUTE) : "0" + calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND) > 9 ? "" + calendario.get(Calendar.SECOND) : "0" + calendario.get(Calendar.SECOND);
        Etiqueta.setText(hora + ":" + minutos + ":" + segundos + ":" + ampm);

}
    
}
