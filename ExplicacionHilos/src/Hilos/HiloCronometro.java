package Hilos;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author CCNAR
 */
public class HiloCronometro extends Thread {

    JLabel EtiquetaCronometro;
    
     //VARIABLES
    
    int Hora = 0, minutos = 0, segundos = 0, Milisegundos = 0, ContaTiempo = 0;
    String TextHora = "", TextMin = "", TextSegun = "", TextMiliSegun = "", Cronometro = "";
    
    @Override
    public void run() {   //Metodo Principal, donde se corre el cronometro.
        try {
            while (true) {
                ContaTiempo++;
                iniciarCronometro(ContaTiempo);
                Thread.sleep(10);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(HiloCronometro.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void iniciarCronometro(int tiempo) {
        if (tiempo >= 59) {
            segundos++;
            ContaTiempo = 0;
        } else if (segundos > 60) {
            minutos++;
            segundos = 0;
        } else if (minutos > 60) {
            Hora++;
            minutos = 0;
        }
        if (tiempo <= 9) {
            TextMiliSegun = "0" + ContaTiempo;
        } else {
            TextMiliSegun = "" + ContaTiempo;
        }
        if (segundos <= 9) {
            TextSegun = "0" + segundos;
        } else {
            TextSegun = "" + segundos;
        }
        if (minutos <= 9) {
            TextMin = "0" + minutos;
        } else {
            TextMin = "" + minutos;
        }
        if (Hora <= 9) {
            TextHora = "0" + Hora;
        } else {
            TextHora = "" + Hora;
        }

        Cronometro = TextHora + " : " + TextMin + " : " + TextSegun + " : " + TextMiliSegun;
        EtiquetaCronometro.setText(Cronometro);
    }
    public void Jlabel (JLabel Etiqueta){
        this.EtiquetaCronometro = Etiqueta ;
        
    }
    public void LimpiarLabel(){
        ContaTiempo = 0; Hora = 0; minutos = 0; segundos = 0; Milisegundos = 0;
        Cronometro = TextHora + " : " + TextMin + " : " + TextSegun + " : " + TextMiliSegun;
        this.EtiquetaCronometro.setText("00 : 00 : 00 : 00 ");
    }
    
}


