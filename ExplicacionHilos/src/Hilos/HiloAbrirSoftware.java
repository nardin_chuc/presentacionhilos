
package Hilos;

/**
 *
 * @author CCNAR
 */
public class HiloAbrirSoftware implements Runnable {
    
    private String Programa;

    @Override
    public void run() {
        try {
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec(Programa);
            p.waitFor();
        } catch (Throwable e) {
            System.err.println("Error al abrir el documento: " + e.getMessage());
        }
    }

    public void setPrograma(String Programa) {
        this.Programa = Programa;
    }
    
    
}
